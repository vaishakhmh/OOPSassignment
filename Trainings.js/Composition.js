/*Composition is a special case of aggregation. 
Composition is when an object contains another object and the contained object can’t live without the container object.*/

/*Books program to demonstrate Composition Concept*/

let Book = {
    "title": "The Alchemist", 
    "author": "Paulo Coelho",
    "publication": {
       "name": "new publication Inc",
       "address": "chennai"
    }
 }

 /*Here the property publication is strictly bounded with the Book object, and publication can’t live without theBook object. 
 If the Book object id was deleted, then the publication would also be deleted*/


 console.log(Book.publication);
 /*gives output only with Book objects instance*/