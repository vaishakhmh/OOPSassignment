

/*Problem-Books Program to demonstate Aggregation concept*/

/*Aggregation is a special case of an association. In the relationship between two objects, one object can have a more major role than the other. In other words, when an object takes more ownership than another one, 
that is aggregation. The owner object is often called the aggregate and the owned object is called the component.*/

function Book(title, author) { 
    this.title = title; 
    this.author = author; 
 }
 const book1 = new Book ('Hippie', 'Paulo Coelho');
 const book2 = new Book ('The Alchemist', 'Paulo Coelho');
 let publication = {
    "name": "new publication Inc", 
    "books": []
 }
 publication.books.push(book1);
 publication.books.push(book2);


 console.log(publication.books);